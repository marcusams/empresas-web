import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme({
  colors: {
    beige: {
      one: "#eeecdb",
      two: "#ebe9d7",
    },
    white: "#ffffff",
    red: {
      neon: "#ff0f44",
    },
    pink: {
      light: "#ee4c77",
      dark: "#c4365b",
    },
    indigo: {
      dark: "#1a0e49",
    },
    rouge: "#991237",
    green: "#7dc075",
    gray: {
      charcoal: "#383743",
      charcoal_two: "#403e4d",
      "300": "#b5b4b4",
      warm: "#8d8c8c",
      "600": "#748383",
    },
    blue: {
      greeny: "#57bbbc",
    },
  },
  fonts: {
    heading: "Roboto",
    body: "Roboto",
  },
  styles: {
    global: {
      body: {
        bg: "beige",
        color: "gray.50",
      },
    },
  },
});
