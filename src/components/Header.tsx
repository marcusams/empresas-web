import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Flex, Icon, Image, Text } from "@chakra-ui/react";

import logoNav from "../assets/logo-nav.png";

import { SearchInput } from "./SearchInput";

import { HiArrowLeft, HiSearch } from "react-icons/hi";

export default function Header({ enterprise, search, setSearch }: any) {
  const [showSearch, setShowSearch] = useState(false);

  const navigate = useNavigate();

  return (
    <Flex
      as="header"
      w="100%"
      bgGradient="linear(to-b, pink.light, pink.dark)"
      height="20"
      mx="auto"
      justify="center"
      align="center"
      px="6"
      py="3"
    >
      <Flex width="100%" alignItems="center" maxWidth={1024}>
        {enterprise ? (
          <>
            <Icon
              as={HiArrowLeft}
              w={5}
              onClick={() => navigate(-1)}
              cursor="pointer"
              h={5}
              color="white"
              fontSize={20}
            />
            <Text mt={0.5} ml={2}>
              {enterprise?.enterprise_name}
            </Text>
          </>
        ) : (
          <>
            {!showSearch && (
              <>
                <Image margin="0 auto" w={40} src={logoNav} alt="Logo Ioasys" />
                <Icon
                  as={HiSearch}
                  w={5}
                  cursor="pointer"
                  h={5}
                  onClick={() => setShowSearch(true)}
                  color="white"
                  fontSize={20}
                />
              </>
            )}
            {showSearch && (
              <SearchInput
                mt={6}
                setValue={setSearch}
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                placeholder="Pesquisar"
              />
            )}
          </>
        )}
      </Flex>
    </Flex>
  );
}
