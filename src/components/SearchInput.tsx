import {
  Flex,
  Icon,
  Input,
  InputProps as ChakraInputProps,
} from "@chakra-ui/react";
import { HiSearch, HiX } from "react-icons/hi";

interface InputProps extends ChakraInputProps {
  setValue: (value: string) => void;
}

export function SearchInput({
  value,
  onChange,
  placeholder,
  setValue,
  mt,
}: InputProps) {
  return (
    <Flex
      mt={mt}
      as="label"
      w="100%"
      p="1"
      borderBottom="1px"
      borderColor="white"
    >
      <Icon
        as={HiSearch}
        w={5}
        cursor="pointer"
        h={5}
        color="white"
        fontSize={20}
      />
      <Input
        color="white"
        variant="unstyled"
        px="3"
        defaultValue={value || ""}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        _placeholder={{
          fontSize: "18px",
          color: "rouge",
          opacity: "0.5",
        }}
      />
      {value && (
        <Icon
          as={HiX}
          onClick={() => setValue("")}
          w={5}
          cursor="pointer"
          h={5}
          color="white"
          fontSize={20}
        />
      )}
    </Flex>
  );
}
