import { Flex, Stack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export function CompanyItem({ item }: any) {
  const navigate = useNavigate();

  return (
    <Flex
      onClick={() => navigate(`/enterprises/${item.id}`)}
      bg="white"
      cursor="pointer"
      _hover={{ opacity: 0.9, transition: "0.5s", transform: "scale(1.1)" }}
      w="100%"
      p="7"
    >
      <Stack spacing="6" width="100%" direction={{ base: "column", sm: "row" }}>
        <Flex
          h="40"
          width={{ base: "100%", sm: "72" }}
          alignItems="center"
          justifyContent="center"
          bg="green"
        >
          <Text textTransform="uppercase" fontWeight="bold" fontSize="6xl">
            {item.enterprise_name.substr(0, 1)}
            {item.enterprise_name.slice(-1)}
          </Text>
        </Flex>
        <Flex justifyContent="center" flexDir="column">
          <Text fontSize="20px" fontWeight="bold" color="indigo.dark">
            {item.enterprise_name}
          </Text>
          <Text color="gray.warm">
            {item.enterprise_type.enterprise_type_name}
          </Text>
          <Text color="gray.warm">{`${item.city} - ${item.country}`}</Text>
        </Flex>
      </Stack>
    </Flex>
  );
}
