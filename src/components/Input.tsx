import {
  Flex,
  Icon,
  Input,
  InputProps as ChakraInputProps,
} from "@chakra-ui/react";
import { HiExclamationCircle } from "react-icons/hi";

interface InputProps extends ChakraInputProps {
  iconLeft: any;
  iconRight?: any;
  error?: any;
  register: any;
  clickIcon?: any;
  name: string;
}

export function CustomInput({
  name,
  placeholder,
  error,
  iconLeft,
  iconRight,
  clickIcon,
  register,
  type,
}: InputProps) {
  return (
    <Flex
      as="label"
      w="100%"
      py="1"
      mt="10"
      borderBottom="1px"
      borderColor={error[name] ? "red.neon" : "gray.charcoal"}
    >
      <Icon
        as={iconLeft}
        w={5}
        cursor="pointer"
        h={5}
        color="pink.light"
        fontSize={20}
      />
      <Input
        color="gray.charcoal_two"
        variant="unstyled"
        autocomplete="off"
        type={type}
        px="3"
        placeholder={placeholder}
        _placeholder={{
          fontSize: "18px",
          color: "gray.charcoal",
          opacity: "0.5",
        }}
        {...register(name, { required: true })}
      />
      {iconRight || error[name] ? (
        <Icon
          as={error[name] ? HiExclamationCircle : iconRight}
          onClick={clickIcon}
          w={6}
          cursor="pointer"
          h={6}
          color={error[name] ? "red.neon" : "gray.charcoal"}
          opacity={error[name] ? "1" : "0.5"}
          fontSize={20}
        />
      ) : null}
    </Flex>
  );
}
