import { useState, useCallback, useContext, createContext } from "react";
import axios from "axios";

const EnterprisesContext = createContext({} as any);

export const EnterprisesProvider = ({ children }: any) => {
  const [loading, setLoading] = useState(false);

  const [enterprises, setEnterprises] = useState<[] | null>(null);
  const [enterprise, setEnterprise] = useState({});

  const getEnterprises = useCallback(async (search, headers) => {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://empresas.ioasys.com.br/api/v1/enterprises",
        {
          headers: JSON.parse(headers),
          params: { name: search },
        }
      );

      setEnterprises(response.data.enterprises);

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  const getEnterprise = useCallback(async (id, headers) => {
    try {
      setLoading(true);
      const response = await axios.get(
        `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`,
        {
          headers: JSON.parse(headers),
        }
      );

      setEnterprise(response.data.enterprise);
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  return (
    <EnterprisesContext.Provider
      value={{
        getEnterprises,
        enterprises,
        getEnterprise,
        setEnterprises,
        enterprise,
        loading,
      }}
    >
      {children}
    </EnterprisesContext.Provider>
  );
};

export function useEnterprises() {
  const context = useContext(EnterprisesContext);

  if (!context)
    throw new Error(
      "useEnterprises must be used within an EnterprisesProvider"
    );

  return context;
}
