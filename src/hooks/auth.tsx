import { useState, useCallback, useContext, createContext } from "react";
import axios from "axios";

const AuthContext = createContext({} as any);

export const AuthProvider = ({ children }: any) => {
  const [loading, setLoading] = useState(false);

  const signIn = useCallback(async ({ email, password }) => {
    const { headers } = await axios.post(
      "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
      { email, password }
    );

    const userData = {
      "access-token": headers["access-token"],
      uid: headers.uid,
      client: headers.client,
    };

    localStorage.setItem("Login", JSON.stringify(userData));
  }, []);

  return (
    <AuthContext.Provider
      value={{
        signIn,
        loading,
        setLoading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  if (!context) throw new Error("useAuth must be used within an AuthProvider");

  return context;
}
