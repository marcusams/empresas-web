import { BrowserRouter, Routes, Route } from "react-router-dom";

import { EnterprisesProvider } from "../hooks/enterprises";
import { AuthProvider } from "../hooks/auth";

import { Enterprise } from "./Enterprise";
import { Dashboard } from "./Enterprises";

import SignIn from "./SignIn";

function App() {
  return (
    <AuthProvider>
      <EnterprisesProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<SignIn />} />
            <Route path="/enterprises" element={<Dashboard />} />
            <Route path="/enterprises/:id" element={<Enterprise />} />
          </Routes>
        </BrowserRouter>
      </EnterprisesProvider>
    </AuthProvider>
  );
}

export default App;
