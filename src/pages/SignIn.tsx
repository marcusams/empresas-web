import { useState } from "react";

import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";

import { Flex, Text, Button, FormControl, Spinner } from "@chakra-ui/react";
import { Image } from "@chakra-ui/react";

import {
  HiEye,
  HiEyeOff,
  HiOutlineLockOpen,
  HiOutlineMail,
} from "react-icons/hi";

import logo from "../assets/logo-home.png";
import { CustomInput } from "../components/Input";
import { useAuth } from "../hooks/auth";

export default function SignIn() {
  const [showPs, setShowPs] = useState(false);

  const navigate = useNavigate();

  const { signIn, loading, setLoading } = useAuth();

  const {
    handleSubmit,
    register,
    watch,
    setError,
    clearErrors,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    try {
      setLoading(true);
      await signIn(data);
      setLoading(false);
      navigate("/enterprises");
    } catch (err: any) {
      setLoading(false);
      if (!err?.response?.success) {
        setError("credentials", {
          type: "manual",
          message: "Credenciais informadas são inválidas, tente novamente.",
        });
      }
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl>
          <Flex w="100%" maxWidth={348} position="relative" h="100vh" mx="auto">
            {loading && (
              <Spinner
                w={20}
                h={20}
                position="absolute"
                thickness="5px"
                speed="0.75s"
                size="xl"
                top="45%"
                color="blue.greeny"
                left="38.5%"
              />
            )}
            <Flex
              filter="auto"
              blur={loading ? "2px" : "0"}
              flexDir="column"
              justify="center"
              align="center"
              w="100%"
            >
              <Image src={logo} alt="Logo Ioasys" />
              <Text
                fontSize="25px"
                textAlign="center"
                mt="16"
                color="gray.charcoal"
                fontWeight="bold"
                letterSpacing="tight"
                w="64"
              >
                BEM-VINDO AO EMPRESAS
              </Text>
              <Text
                fontSize="17px"
                textAlign="center"
                mt="6"
                color="gray.charcoal"
                letterSpacing="tight"
              >
                Lorem ipsum dolor sit amet, contetur <br /> adipiscing elit.
                Nunc accumsan.
              </Text>
              <CustomInput
                name="email"
                placeholder="E-mail"
                register={register}
                iconLeft={HiOutlineMail}
                error={errors}
              />
              <CustomInput
                name="password"
                placeholder="Senha"
                register={register}
                iconLeft={HiOutlineLockOpen}
                iconRight={
                  watch("password") ? (showPs ? HiEye : HiEyeOff) : null
                }
                type={showPs ? "text" : "password"}
                clickIcon={() => setShowPs(!showPs)}
                error={errors}
              />
              {errors.credentials && (
                <Text
                  fontSize="13px"
                  textAlign="center"
                  color="red.neon"
                  mt="6"
                  letterSpacing="tight"
                >
                  Credenciais informadas são inválidas, tente novamente.
                </Text>
              )}

              <Flex as="label" w="100%" py="1" mt="10">
                <Button
                  type="submit"
                  borderRadius="3.6px"
                  onClick={() => clearErrors("credentials")}
                  backgroundColor="blue.greeny"
                  width="100%"
                  height="52px"
                  _hover={{ opacity: "0.7" }}
                >
                  ENTRAR
                </Button>
              </Flex>
            </Flex>
          </Flex>
        </FormControl>
      </form>
    </>
  );
}
