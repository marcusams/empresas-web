import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";

import { useEnterprises } from "../hooks/enterprises";

import { Flex, Stack, Text } from "@chakra-ui/react";
import { CompanyItem } from "../components/Company/CompanyItem";
import Header from "../components/Header";

export function Dashboard() {
  const headers = localStorage.getItem("Login") as any;

  const { enterprises, getEnterprises } = useEnterprises();

  const [search, setSearch] = useState("");

  useEffect(() => {
    if (search) {
      const timeoutId = setTimeout(() => {
        getEnterprises(search, headers);
      }, 400);

      return () => clearTimeout(timeoutId);
    }
  }, [getEnterprises, headers, search]);

  return !localStorage.getItem("Login") ? (
    <Navigate to="/" />
  ) : (
    <>
      <Header search={search} setSearch={setSearch} />

      <Flex
        maxWidth={1024}
        margin="0 auto"
        p="12"
        flexDir="column"
        width="100%"
        justifyContent="center"
      >
        {enterprises && (
          <Stack spacing="6" flexDir="column">
            {enterprises.map((item: any, index: any) => (
              <CompanyItem item={item} index={index} />
            ))}
          </Stack>
        )}

        {!enterprises && (
          <Text mt={250} textAlign="center" color="gray.charcoal" fontSize="md">
            Clique na busca para iniciar.
          </Text>
        )}

        {enterprises?.length === 0 && (
          <Text mt={150} textAlign="center" color="gray.300" fontSize="md">
            Nenhuma empresa foi encontrada para a busca realizada.
          </Text>
        )}
      </Flex>
    </>
  );
}
