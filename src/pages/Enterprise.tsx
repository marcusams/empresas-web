import { useEffect } from "react";
import { Navigate, useParams } from "react-router-dom";

import { useEnterprises } from "../hooks/enterprises";

import { Box, Flex, Stack, Text } from "@chakra-ui/react";

import Header from "../components/Header";

export function Enterprise() {
  const { id } = useParams();

  const headers = localStorage.getItem("Login") as any;

  const { enterprise, getEnterprise, setEnterprises } = useEnterprises();

  useEffect(() => {
    if (id) {
      getEnterprise(id, headers);
      setEnterprises(null);
    }
  }, [getEnterprise, setEnterprises, id, headers]);

  return !localStorage.getItem("Login") ? (
    <Navigate to="/" />
  ) : (
    <>
      <Header enterprise={enterprise} />
      <Box p="12">
        <Flex bg="white" w="100%" p="7" maxWidth={1024} margin="0 auto">
          <Stack spacing="6" width="100%" direction={{ base: "column" }}>
            <Flex
              h="40"
              width={{ base: "100%" }}
              alignItems="center"
              justifyContent="center"
              bg="green"
            >
              <Text textTransform="uppercase" fontWeight="bold" fontSize="6xl">
                {enterprise?.enterprise_name?.substr(0, 1)}
                {enterprise?.enterprise_name?.slice(-1)}
              </Text>
            </Flex>
            <Flex justifyContent="center" flexDir="column">
              <Text fontSize="16px" color="gray.warm">
                {enterprise?.description}
              </Text>
            </Flex>
          </Stack>
        </Flex>
      </Box>
    </>
  );
}
